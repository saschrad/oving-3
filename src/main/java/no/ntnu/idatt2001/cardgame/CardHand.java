package no.ntnu.idatt2001.cardgame;

import java.util.ArrayList;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Represents a player hand. A player hand stores PlayingCards that the player got.
 * The class also managed method to check the check the hand for specific combinations like a flush
 */
public class CardHand {
    private ArrayList<PlayingCard> cards;

    public CardHand() {
        cards = new ArrayList<>();
    }

    /**
     * Adds a single card to the players deck.
     * @param card PlayingCard to be added
     */
    public void addCard(PlayingCard card) {
        cards.add(card);
    }

    /**
     * Adds all cards in an arraylist to the deck
     * @param cardList Arraylist of PlayingCards
     */
    public void addCards(ArrayList<PlayingCard> cardList) {
        cardList.forEach(c -> cards.add(c));
    }

    /**
     * Method to get all cards that the player got.
     * @return ArraList of PlayingCards
     */
    public ArrayList<PlayingCard> getCards() {
        return cards;
    }

    /**
     * Returns all cards of a specific suit.
     * @param type suit
     * @return Arraylist of PlayingCards
     */
    public ArrayList<PlayingCard> getAllWithSuit(String type) {
        return cards.stream().filter(c -> Character.toString(c.getSuit()).equalsIgnoreCase(type)).collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * Gets a sum of all the cards in the deck
     *  <b>Type - Value</b>
     *  Ace - 1
     *  Two - 2
     *  ...
     *  Jack - 11
     *  Queen - 12
     *  King 13
     * @return
     */
    public int getCardSum() {
        return cards.stream().map(PlayingCard::getFace).reduce(0, Integer::sum);
    }

    /**
     * Checks if the deck contains a single card
     * @param type card in the format "SuitFace" e.g "D12"
     * @return true or false dependent on if the deck contains the card.
     */
    public boolean containsCard(String type) {
        return cards.stream().anyMatch(c -> (c.getSuit()+""+c.getFace()).equals(type));
    }

    /**
     * Checks if the deck contains a flush.
     * A flush is a hand that contains at least 5 cards of the same suit.
     * @return true or false dependent on if the deck got a flush
     */
    public boolean flush() {
        Map<String, Long> count = cards.stream().map(c -> Character.toString(c.getSuit())).collect(Collectors.groupingBy(e -> e, Collectors.counting()));
        return count.values().stream().anyMatch(l -> l >= 5);
    }
}
