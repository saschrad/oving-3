package no.ntnu.idatt2001.cardgame.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import no.ntnu.idatt2001.cardgame.CardHand;
import no.ntnu.idatt2001.cardgame.DeckOfCards;
import no.ntnu.idatt2001.cardgame.PlayingCard;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class MainController {
    @FXML Button Test;
    @FXML Button checkHand;
    @FXML VBox cardsVbox;
    @FXML Text sumOfFaces;
    @FXML Text flush;
    @FXML Text hearts;
    @FXML Text queenOfSpades;
    @FXML ImageView cardImage;

    DeckOfCards deck = new DeckOfCards();
    CardHand hand = new CardHand();

    /**
     * Initialises the app and fills it with data
     */
    public void initialize() {
        cardsVbox.getChildren().clear();
        this.addCard(hand.getCards());
    }

    /**
     * Method that draws 4 cards and adds it into the deck
     * @param event
     * @throws IOException
     */
    public void buttonDrawCards(ActionEvent event) throws IOException {
        hand.addCards(deck.dealHand(4));
        this.initialize();
    }

    /**
     * Method that managed checking if the deck got a flush, a spesific card (S12) and that finds all hearts and the sum. The method showing this data in the application
     */
    public void buttonCheckHand() {
        boolean isFlush = hand.flush();
        boolean containsCard = hand.containsCard("S12");
        String heartsList = hand.getAllWithSuit("H").stream().map(c -> c.getSuit() + "" + c.getFace()).collect(Collectors.joining(" "));
        int sum = hand.getCardSum();

        flush.setText(isFlush ? "Yes": "No");
        hearts.setText(heartsList);
        queenOfSpades.setText(containsCard ? "Yes" : "No");
        sumOfFaces.setText(sum + "");
    }

    /**
     * Adds a card to the vbox.
     * @param cardObject
     * @throws IOException
     */
    public void addCard(PlayingCard cardObject) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/card.fxml"));
        Pane card = loader.load();
        CardController cardController = loader.getController();

        cardController.setText(cardObject.getFace() + "" + cardObject.getSuit());
        cardController.setImage(cardObject.getFace(), cardObject.getSuit());
        cardsVbox.getChildren().add( cardsVbox.getChildren().size(),card);
    }

    /**
     * Takes a list of cards and display them to the vbox
     * @param cardList list of cards
     */
    public void addCard(ArrayList<PlayingCard> cardList) {
        if(cardList != null) {
            cardList.forEach(c -> {
                try {
                    addCard(c);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }
}
