package no.ntnu.idatt2001.cardgame.controllers;

import javafx.fxml.FXML;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;

/**
 * Class that manges card.fxml.
 */
public class CardController {
    private final String suit = "CDHS";
    private final int IMAGE_WIDTH = 79;
    private final int IMAGE_HEIGHT = 123;

    @FXML Text cardText;
    @FXML ImageView cardImg;

    /**
     * Sets a string to the text in card.fxml
     * @param text string to be added in the Text field.
     */
    public void setText(String text){
        cardText.setText(text);
    }

    /**
     * Method that creates and sets a card image ImageView.
     * @param face face of the card (1-13)
     * @param suit suit of the card (C,D,H or S)
     */
    public void setImage(int face, char suit) {
        if(face < 1 || face > 13) throw new IllegalArgumentException("Face needs to be between 1 (Ace) and 13 (King)");
        if(!this.suit.contains(suit + "")) throw new IllegalArgumentException("The suit can only be C, D, H or S");

        Image wholeImg = new Image("./cardsSheet2.png");
        cardImg.setImage(wholeImg);
        Rectangle2D imagePart = new Rectangle2D((face - 1) * IMAGE_WIDTH, (this.suit.indexOf(suit)) * IMAGE_HEIGHT, IMAGE_WIDTH, IMAGE_HEIGHT);
        cardImg.setViewport(imagePart);
    }
}
