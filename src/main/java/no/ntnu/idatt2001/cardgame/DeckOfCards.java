package no.ntnu.idatt2001.cardgame;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;
import java.util.stream.IntStream;

/**
 * Represents a deck of cards. A deck contains 52 cards. 4 types and 13 faces.
 * Types: S, H, D, C
 * Faces: Ace, 1, 2, ... , 10, Jack, Queen, King
 */
public class DeckOfCards {
    private final char[] suit = {'S', 'H', 'D', 'C'};
    private ArrayList<PlayingCard> deck = new ArrayList<>();

    /**
     * Creates a deck of 52 cards
     */
    public DeckOfCards () {
        for(char face: suit) {
            for(int i = 1; i <= 13; i++) {
                deck.add(new PlayingCard(face, i));
            }
        }
    }

    /**
     * Get all the cards remaining inside the deck
     * @return Arraylist of PlayingCards
     */
    public ArrayList<PlayingCard> getDeck() {
        return deck;
    }

    /**
     * Takes n random cards from the deck and returns them in a list. These cards are then removed from the deck.
     * n needs to be between 1 and 52. Else it will throw and IllegalArgumentException.
     * If n is greater than the amount of cards left in the deck you will draw the remaining cards. If there is no cards
     * left in deck it will throw an ArrayIndexOutOfBoundsException
     *
     * @param n amount of cards that will be taken
     * @return ArrayList of PlayingCards taken from the deck
     */
    public ArrayList<PlayingCard> dealHand(int n) {
        if(n <= 0) throw new IllegalArgumentException("Invalid input: The input needs to be between 1 and 52");
        if(deck.size() == 0) throw new ArrayIndexOutOfBoundsException("No cards left in deck. All cards have been picked");
        Random random = new Random();
        ArrayList<PlayingCard> cards = new ArrayList<>();

        // Pick cards for n or until there are no cards in the deck.
        IntStream.range(0, Math.min(n, deck.size())).forEach(i -> {
            PlayingCard card = deck.get(random.nextInt(deck.size()));
            cards.add(card);
            deck.remove(card);
        });

        return cards;
    }
}
