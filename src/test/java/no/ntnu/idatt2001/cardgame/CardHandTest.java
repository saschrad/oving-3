package no.ntnu.idatt2001.cardgame;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class CardHandTest {
    @Test
    public void addCardToHandTest() {
        CardHand hand = new CardHand();
        hand.addCard(new PlayingCard('D', 12));
        assertEquals("D12", hand.getCards().get(0).getAsString());
    }

    @Test
    public void isFlushTest() {
        // Flush
        CardHand hand = new CardHand();
        IntStream.range(0,5).mapToObj(i -> new PlayingCard('D', i +1 )).forEach(hand::addCard);
        assertTrue(hand.flush());

        // Not Flush
        CardHand hand2 = new CardHand();
        IntStream.range(0,4).mapToObj(i -> new PlayingCard('D', i +1 )).forEach(hand2::addCard);
        hand.addCard(new PlayingCard('S', 5));
        assertFalse(hand2.flush());
    }

    @Test
    public void getCardSumTest() {
        CardHand hand = new CardHand();

        // No cards
        assertEquals(0, hand.getCardSum());

        // Ace - King => 1+2+3+...+13
        IntStream.range(0,13).mapToObj(i -> new PlayingCard('D', i +1 )).forEach(hand::addCard);
        int total = 1+2+3+4+5+6+7+8+9+10+11+12+13;
        assertEquals(total, hand.getCardSum());
    }

    @Test
    public void containsCardTest() {
        CardHand hand = new CardHand();

        // Card not existing
        assertFalse(hand.containsCard("3dsada"));

        // Contains D4;
        hand.addCard(new PlayingCard('D', 4));
        assertTrue(hand.containsCard("D4"));
    }

    @Test
    public void getAllWithSuit() {
        CardHand hand = new CardHand();
        String suit = "CDHS";

        // Create a random list of cards with random suit
        ArrayList<PlayingCard> cards = IntStream.range(0,13)
                .mapToObj(i -> new PlayingCard(suit.charAt((int) Math.floor(Math.random()*suit.length())), i+1))
                .collect(Collectors.toCollection(ArrayList::new));
        hand.addCards(cards);

        // Filter the card arraylist and get all diamonds. Then map it into an list of strings then collect it and make a string from the list
        String diamonds = cards.stream().filter(c -> c.getSuit() == 'D').map(c -> c.getSuit() + "").collect(Collectors.joining(""));

        // Get filtered arraylist with just diamond cards from hand. Then use map so we get a list of just the suits in list of strings. Then make it into a string
        String diamondsFromHand = hand.getAllWithSuit("D").stream().map(c -> c.getSuit() + "").collect(Collectors.joining(""));

        // Check if strings are equal. This means equal length and same characters. E.g "DDDD" === "DDDD" -> true
        assertEquals(diamonds, diamondsFromHand);
    }

}