package no.ntnu.idatt2001.cardgame;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class DeckOfCardsTest {

    @Test
    public void DeckSizeAndContentTest(){
        DeckOfCards d = new DeckOfCards();
        assertEquals(52, d.getDeck().size());
        assertNotEquals(d.getDeck().get(0), d.getDeck().get(51));
    }

    @Test
    public void DealHandTest() {
        DeckOfCards d = new DeckOfCards();
        ArrayList<PlayingCard> cards = d.dealHand(10);

        // Draw 10 cards
        assertEquals(10, cards.size());

        // draw negative card
        assertThrows(IllegalArgumentException.class, () -> d.dealHand(-1));
    }

    @Test
    public void DrawCardsTest() {
        DeckOfCards d = new DeckOfCards();

        // Draw all cards in deck
        assertDoesNotThrow(() -> {
            d.dealHand(52);
        });

        // Draw more cards
        assertThrows(IndexOutOfBoundsException.class, () -> d.dealHand(1));
    }
}